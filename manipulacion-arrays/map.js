numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const multiplyValues = ($array, $multiplicador) =>
  $array.map((item) => item * $multiplicador);
console.log(multiplyValues(numeros, 2));

// Calula y agrega nuevas propiedades a un array

elementos = [
  {
    name: 'Product 1',
    price: 1000,
    stock: 10,
  },
  {
    name: 'Product 2',
    price: 2000,
    stock: 20,
  },
];

const addNewAttr = ($array) =>
  $array.map((item) => ({ ...item, taxes: Math.trunc(item.price * 0.19) }));

console.log(addNewAttr(elementos));

const array = [{ age: 1 }, { age: 2 }];
const rta = array.map((item) => {
  item.name = 'My name';
  return item;
});

console.log('Cambiar Valores de un array:', rta);
