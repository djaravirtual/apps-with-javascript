const elementos = ['fire', 'Air', 'water'];
const rpta = elementos.join('--');
console.log('join:', rpta);

const title = 'Curso de manipulación de arrays';

const rpta2 = title.split(' ');
const rpta3 = title.split(' ').join('-');

console.log('split:', rpta2);
console.log('split+join:', rpta3);

const input = 'La FORMA de correr Python';
console.log('URL Amigagle:', input.toLowerCase().split(' ').join('-'));
