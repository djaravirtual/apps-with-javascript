// some => al menos 1 elemto debe cumplir la condición
numeros = [1, 2, 3, 4, 5, 6, 7, 8];
let rpta = numeros.some((item) => item % 2 === 0);

console.log(rpta);

// every => todos los elementos deben cumplir la condición

numeros2 = [1, 2, 3, 4, 5, 6, 7, 8];
let rpta2 = numeros2.every((item) => item > 4);
console.log(rpta2);

const team = [
  {
    name: 'Nicolas',
    age: 12,
  },
  {
    name: 'Andrea',
    age: 8,
  },
  {
    name: 'Zulema',
    age: 2,
  },
  {
    name: 'Santiago',
    age: 18,
  },
];

let team_menores_a15 = team.every((item) => item.age > 15);
console.log('Equipo con miembres mayores de 15=', team_menores_a15);
