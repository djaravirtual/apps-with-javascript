let a = [1, 1, 2, 2];
let b = [3, 3, 4, 4];

let result = a.concat(b);

console.log(result);

// quitar elementos duplicados de un array

// Usando filter con indexOf
let result1 = a.filter((item, index) => a.indexOf(item) !== index);
console.log('Array (a) sin repetidos:', result1);

// Usando reduce
let result2 = b.reduce((newarray, item) => {
  if (!newarray.includes(item)) {
    newarray.push(item);
  }
  return newarray;
}, []);
console.log('Array (b) sin repetidos:', result2);

// Concat
console.log('Concat:', result1.concat(result2));

numeros2 = [8, 2, 9, 4, 5, 6, 7, 8];

numeros2.sort((a, b) => b - a);
numeros3 = numeros2;
console.log(numeros3);
