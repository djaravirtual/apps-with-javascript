// Find => retorna un objeto que hizo matcher, solo devuelve el 1er valor

numeros2 = [1, 2, 3, 4, 5, 6, 7, 8];
let rpta2 = numeros2.find((item) => item === 4);
console.log(rpta2);

const products = [
  {
    name: 'Pizza',
    price: 12,
    id: '🍕',
  },
  {
    name: 'Burger',
    price: 23,
    id: '🍔',
  },
  {
    name: 'Hot dog',
    price: 34,
    id: '🌭',
  },
  {
    name: 'Hot cakes',
    price: 355,
    id: '🥞',
  },
];

let rpta3 = products.find((item) => item.id === '🍔');
console.log('first:', rpta3);

array = ['ana', 'santi', 'nico', 'anastasia'];
term = 'xyz';

console.log(
  'includes:',
  array.filter((item) => item.includes(term))
);
