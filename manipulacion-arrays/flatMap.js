let input = [
  'Beautiful is better than ugly',
  'Explicit is better than implicit',
  'Simple is better than complex',
  'Complex is better than complicated',
];

console.log(
  'Separar Palabras: ',
  input.flatMap((e) => e.split(' '))
);
console.log(
  'Contar el nro de palabras: ',
  input.flatMap((e) => e.split(' ')).length
);

const array = [['🐸', '🐱'], '🐹', ['🐯']];
const rta = array.flat();

console.log('flat;', rta);
