const API_KEY = "18b329be-de74-43f1-9e3a-996a403c9b7a";
// Set config defaults when creating the instance
const api = axios.create({
  baseURL: "https://api.thecatapi.com/V1",
  timeout: 5000,
});

// Alter defaults after instance has been created
api.defaults.headers.common["x-api-key"] = API_KEY;

const API_URL_RANDOM = "https://api.thecatapi.com/V1";

const spanMsg = document.getElementById("msg");

function showMsg(msg, type = "error") {
  spanMsg.style.display = "block";
  spanMsg.innerText = msg;
  spanMsg.className =
    type == "error" ? "alert alert-danger" : "alert alert-success";

  // Despues de 3 segundos ocultal el error
  setTimeout(() => {
    spanMsg.style.display = "none";
  }, 3000);
}

async function loadRandomMichis() {
  const { status, data } = await api.get("/images/search?limit=3");
  //console.info("Loading random michis...");
  console.log(data);
  if (status !== 200) {
    showMsg(`Hubo un error: ${status} ${data.message}`);
  } else {
    const img1 = document.getElementById("img01");
    const img2 = document.getElementById("img02");
    const img3 = document.getElementById("img03");

    const btn1 = document.getElementById("btn1");
    const btn2 = document.getElementById("btn2");
    const btn3 = document.getElementById("btn3");

    img1.src = data[0].url;
    img2.src = data[1].url;
    img3.src = data[2].url;

    btn1.onclick = () => saveFavoritesMichi(data[0].id);
    btn2.onclick = () => saveFavoritesMichi(data[1].id);
    btn3.onclick = () => saveFavoritesMichi(data[2].id);
  }
}

async function loadFavoritesMichis() {
  const { status, data } = await api.get("/favourites?limit=10");
  if (status !== 200) {
    showMsg(`Hubo un error: ${status} ${data.message}`);
  } else {
    const articles = [];
    const section = document.getElementById("FavoriteMichis");
    //const h2 = document.createElement("h2");
    //const h2Text = document.createTextNode("Michis Favoritos");
    //h2.appendChild(h2Text);
    section.innerHTML = "";
    //section.appendChild(h2);
    data.forEach(michi => {
      const art = document.createElement("article");
      const img = document.createElement("img");
      const btn = document.createElement("button");
      const textBtn = document.createTextNode("Eliminar Favorito");
      btn.appendChild(textBtn);
      btn.onclick = () => deleteFavoriteMichi(michi.id);
      btn.className = "btn btn-danger";
      btn.style = "display: block";
      img.src = michi.image.url;
      img.className = "img-thumbnail mb-2 format_image";
      art.className = "col-12 col-sm-4";
      art.append(img, btn);
      articles.push(art);
    });
    section.append(...articles);
  }
}

async function saveFavoritesMichi(idFavorite) {
  let { data, status } = await api.post("/favourites", {
    image_id: idFavorite,
  });

  if (status !== 200) {
    showMsg(`Hubo un error: ${status} ${data.message}`);
  } else {
    showMsg(
      "Favorito agregado correctamente: image_id: " + idFavorite,
      "success"
    );
    loadFavoritesMichis();
  }
}

async function deleteFavoriteMichi(idFavorite) {
  const { status, data } = await api.delete(`/favourites/${idFavorite}`);

  if (status !== 200) {
    showMsg(`Hubo un error: ${status} ${data.message}`);
  } else {
    showMsg(
      "Favorito Borrado correctamente: image_id: " + idFavorite,
      "success"
    );
    loadFavoritesMichis();
  }
}

loadRandomMichis();
loadFavoritesMichis();
